from app.stocks.api import router as stocks_router
from app.portfolios.api import router as portfolios_router


def init_routes(app):
    app.include_router(stocks_router)
    app.include_router(portfolios_router)
