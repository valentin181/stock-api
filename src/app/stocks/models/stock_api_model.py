from pydantic import BaseModel


class StockApiModel(BaseModel):
    ticker: str
    full_name: str
