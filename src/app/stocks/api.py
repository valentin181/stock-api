from fastapi import APIRouter

from .models.stock_api_model import StockApiModel
from data.repository import Repository

router = APIRouter(prefix="/stocks", tags=["Stocks"])

repository = Repository()


@router.post("/")
async def add_stock(stock: StockApiModel):
    return repository.add_stock(stock.ticker, stock.full_name)


@router.put("/{ticker}")
async def update_stock(ticker: str, stock: StockApiModel):
    return repository.update_stock(ticker, stock.ticker, stock.full_name)


@router.get("/{ticker}")
async def get_stock(ticker: str):
    return repository.get_stock(ticker)


@router.get("/")
async def get_stocks():
    return repository.get_all_stocks()
