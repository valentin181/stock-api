from fastapi import APIRouter

router = APIRouter(prefix="/portfolios", tags=["Portfolios"])


@router.get("/")
def get_all_portfolios():
    return {"message": "Bisher sind noch keine Portfolios hinterlegt"}
