import uvicorn
from fastapi import FastAPI

from app import init_routes
from data.database import Base, engine

Base.metadata.create_all(bind=engine)

app = FastAPI()

init_routes(app)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
