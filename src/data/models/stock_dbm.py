from sqlalchemy import Column, Integer, String

from ..database import Base


class StockDbm(Base):
    __tablename__ = "stock"

    id = Column(Integer, primary_key=True, index=True)
    ticker = Column(String, unique=True, index=True)
    full_name = Column(String)
