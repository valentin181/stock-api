from typing import List

from sqlalchemy.orm import Session

from data import SessionLocal
from data.models.stock_dbm import StockDbm


class Repository:
    db: Session

    def __init__(self):
        self.db = SessionLocal()

    def get_all_stocks(self) -> List[StockDbm]:
        return self.db.query(StockDbm).all()

    def get_stock(self, ticker: str) -> StockDbm:
        return self.db.query(StockDbm).filter(StockDbm.ticker == ticker).one()

    def add_stock(self, ticker: str, full_name: str) -> StockDbm:
        stock_dbm = StockDbm(ticker=ticker, full_name=full_name)
        self.db.add(stock_dbm)
        self.db.commit()
        self.db.refresh(stock_dbm)
        return stock_dbm

    def update_stock(self, old_ticker: str, new_ticker: str, full_name: str) -> StockDbm:
        stock_dbm = self.db.query(StockDbm).filter(StockDbm.ticker == old_ticker).one()
        stock_dbm.ticker = new_ticker
        stock_dbm.full_name = full_name
        self.db.add(stock_dbm)
        self.db.commit()
        self.db.refresh(stock_dbm)
        return stock_dbm
